# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

# Write file_shuffler program that: * prompts the user for a file name * reads that file * shuffles the lines * saves it to the file "{input_name}-shuffled.txt".

puts "Which file would you like to use?"
from_file = gets.chomp

array_of_lines = File.readlines(from_file)
shuffled_lines = array_of_lines.shuffle

to_file = File.new("#{from_file}-shuffled.txt", 'w')
to_file.puts(shuffled_lines)
to_file.close
